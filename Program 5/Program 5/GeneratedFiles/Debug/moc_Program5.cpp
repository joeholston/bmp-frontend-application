/****************************************************************************
** Meta object code from reading C++ file 'Program5.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.6.3)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../Program5.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'Program5.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.6.3. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Program5_t {
    QByteArrayData data[10];
    char stringdata0[87];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Program5_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Program5_t qt_meta_stringdata_Program5 = {
    {
QT_MOC_LITERAL(0, 0, 8), // "Program5"
QT_MOC_LITERAL(1, 9, 10), // "sendPixmap"
QT_MOC_LITERAL(2, 20, 0), // ""
QT_MOC_LITERAL(3, 21, 8), // "loadFile"
QT_MOC_LITERAL(4, 30, 8), // "saveFile"
QT_MOC_LITERAL(5, 39, 9), // "closeFile"
QT_MOC_LITERAL(6, 49, 14), // "setCounterText"
QT_MOC_LITERAL(7, 64, 5), // "write"
QT_MOC_LITERAL(8, 70, 4), // "read"
QT_MOC_LITERAL(9, 75, 11) // "aboutDialog"

    },
    "Program5\0sendPixmap\0\0loadFile\0saveFile\0"
    "closeFile\0setCounterText\0write\0read\0"
    "aboutDialog"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Program5[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       8,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   54,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   57,    2, 0x0a /* Public */,
       4,    0,   58,    2, 0x0a /* Public */,
       5,    0,   59,    2, 0x0a /* Public */,
       6,    0,   60,    2, 0x0a /* Public */,
       7,    0,   61,    2, 0x0a /* Public */,
       8,    0,   62,    2, 0x0a /* Public */,
       9,    0,   63,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void, QMetaType::QPixmap,    2,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Program5::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Program5 *_t = static_cast<Program5 *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->sendPixmap((*reinterpret_cast< QPixmap(*)>(_a[1]))); break;
        case 1: _t->loadFile(); break;
        case 2: _t->saveFile(); break;
        case 3: _t->closeFile(); break;
        case 4: _t->setCounterText(); break;
        case 5: _t->write(); break;
        case 6: _t->read(); break;
        case 7: _t->aboutDialog(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (Program5::*_t)(QPixmap );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&Program5::sendPixmap)) {
                *result = 0;
                return;
            }
        }
    }
}

const QMetaObject Program5::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Program5.data,
      qt_meta_data_Program5,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *Program5::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Program5::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_Program5.stringdata0))
        return static_cast<void*>(const_cast< Program5*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Program5::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 8)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 8;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 8)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 8;
    }
    return _id;
}

// SIGNAL 0
void Program5::sendPixmap(QPixmap _t1)
{
    void *_a[] = { Q_NULLPTR, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}
QT_END_MOC_NAMESPACE
