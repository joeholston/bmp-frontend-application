/********************************************************************************
** Form generated from reading UI file 'Program5.ui'
**
** Created by: Qt User Interface Compiler version 5.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PROGRAM5_H
#define UI_PROGRAM5_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Program5Class
{
public:
    QAction *actionOpenFile;
    QAction *actionExit;
    QAction *actionSave;
    QAction *actionClose;
    QAction *actionAbout;
    QWidget *centralWidget;
    QLabel *pictureLabel;
    QPlainTextEdit *textEdit;
    QPushButton *readButton;
    QPushButton *writeButton;
    QLabel *textCounter;
    QLabel *slash;
    QLabel *textCap;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuHelp;

    void setupUi(QMainWindow *Program5Class)
    {
        if (Program5Class->objectName().isEmpty())
            Program5Class->setObjectName(QStringLiteral("Program5Class"));
        Program5Class->resize(590, 600);
        actionOpenFile = new QAction(Program5Class);
        actionOpenFile->setObjectName(QStringLiteral("actionOpenFile"));
        actionExit = new QAction(Program5Class);
        actionExit->setObjectName(QStringLiteral("actionExit"));
        actionSave = new QAction(Program5Class);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClose = new QAction(Program5Class);
        actionClose->setObjectName(QStringLiteral("actionClose"));
        actionAbout = new QAction(Program5Class);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        centralWidget = new QWidget(Program5Class);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pictureLabel = new QLabel(centralWidget);
        pictureLabel->setObjectName(QStringLiteral("pictureLabel"));
        pictureLabel->setGeometry(QRect(10, 10, 571, 401));
        QFont font;
        font.setPointSize(16);
        pictureLabel->setFont(font);
        pictureLabel->setFrameShape(QFrame::WinPanel);
        pictureLabel->setAlignment(Qt::AlignCenter);
        textEdit = new QPlainTextEdit(centralWidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(20, 450, 441, 91));
        readButton = new QPushButton(centralWidget);
        readButton->setObjectName(QStringLiteral("readButton"));
        readButton->setGeometry(QRect(480, 450, 91, 41));
        writeButton = new QPushButton(centralWidget);
        writeButton->setObjectName(QStringLiteral("writeButton"));
        writeButton->setGeometry(QRect(480, 500, 91, 41));
        textCounter = new QLabel(centralWidget);
        textCounter->setObjectName(QStringLiteral("textCounter"));
        textCounter->setGeometry(QRect(20, 420, 16, 16));
        slash = new QLabel(centralWidget);
        slash->setObjectName(QStringLiteral("slash"));
        slash->setGeometry(QRect(40, 420, 16, 16));
        textCap = new QLabel(centralWidget);
        textCap->setObjectName(QStringLiteral("textCap"));
        textCap->setGeometry(QRect(60, 420, 481, 16));
        Program5Class->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(Program5Class);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        Program5Class->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(Program5Class);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        Program5Class->setStatusBar(statusBar);
        menuBar = new QMenuBar(Program5Class);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 590, 21));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QStringLiteral("menuFile"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        Program5Class->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionOpenFile);
        menuFile->addAction(actionSave);
        menuFile->addSeparator();
        menuFile->addAction(actionClose);
        menuFile->addSeparator();
        menuFile->addAction(actionExit);
        menuHelp->addAction(actionAbout);

        retranslateUi(Program5Class);
        QObject::connect(actionExit, SIGNAL(triggered()), Program5Class, SLOT(close()));
        QObject::connect(actionOpenFile, SIGNAL(triggered()), Program5Class, SLOT(loadFile()));
        QObject::connect(Program5Class, SIGNAL(sendPixmap(QPixmap)), pictureLabel, SLOT(setPixmap(QPixmap)));
        QObject::connect(actionClose, SIGNAL(triggered()), Program5Class, SLOT(closeFile()));
        QObject::connect(actionSave, SIGNAL(triggered()), Program5Class, SLOT(saveFile()));
        QObject::connect(writeButton, SIGNAL(clicked()), Program5Class, SLOT(write()));
        QObject::connect(textEdit, SIGNAL(textChanged()), Program5Class, SLOT(setCounterText()));
        QObject::connect(actionAbout, SIGNAL(triggered()), Program5Class, SLOT(aboutDialog()));
        QObject::connect(readButton, SIGNAL(clicked()), Program5Class, SLOT(read()));

        QMetaObject::connectSlotsByName(Program5Class);
    } // setupUi

    void retranslateUi(QMainWindow *Program5Class)
    {
        Program5Class->setWindowTitle(QApplication::translate("Program5Class", "Program5", Q_NULLPTR));
        actionOpenFile->setText(QApplication::translate("Program5Class", "Open File...", Q_NULLPTR));
        actionExit->setText(QApplication::translate("Program5Class", "Exit", Q_NULLPTR));
        actionSave->setText(QApplication::translate("Program5Class", "Save...", Q_NULLPTR));
        actionClose->setText(QApplication::translate("Program5Class", "Close", Q_NULLPTR));
        actionAbout->setText(QApplication::translate("Program5Class", "About...", Q_NULLPTR));
        pictureLabel->setText(QApplication::translate("Program5Class", "Select \"Open\" from the File Menu", Q_NULLPTR));
        readButton->setText(QApplication::translate("Program5Class", "Read", Q_NULLPTR));
        writeButton->setText(QApplication::translate("Program5Class", "Write", Q_NULLPTR));
        textCounter->setText(QApplication::translate("Program5Class", "0", Q_NULLPTR));
        slash->setText(QApplication::translate("Program5Class", "/", Q_NULLPTR));
        textCap->setText(QApplication::translate("Program5Class", "0", Q_NULLPTR));
        menuFile->setTitle(QApplication::translate("Program5Class", "File", Q_NULLPTR));
        menuHelp->setTitle(QApplication::translate("Program5Class", "Help", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class Program5Class: public Ui_Program5Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PROGRAM5_H
