/*
Author: Joe Holston- COMP 322 
Program: BMP Frontend Application
Purpose: To provide a frontend application that will open and save files, using a provided DLL, and write and read messages into an image using the method of steganography
Date: November 30, 2017
*/

#include "Program5.h"
#include "BMP_Handler.h"
#include <qfiledialog.h>
#include <qstring.h>
#include <qmessagebox.h>

Program5::Program5(QWidget *parent): QMainWindow(parent)
{
	ui.setupUi(this);
	disable();
}

//loadFile() utilizes the BMP_Handler DLL to load bmp files into a char* array. It also test the capacity and enables the edit textbox and buttons
void Program5::loadFile() 
{
	QString filename = QFileDialog::getOpenFileName(this, tr("Open File"), QString(), tr("Images *.bmp"));
	emit sendPixmap(QPixmap(filename));
	ui.pictureLabel->setScaledContents(true);
	ui.pictureLabel->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	enable();
	BMP_Handler bmp;
	fname = filename.toLocal8Bit().constData();
	bits = bmp.loadBMP(fname.c_str(), width, height);
	capacity = ((width*height) / 3) - 8;
	ui.textCap->setText(QString::number(capacity));
}

//saveFile() opens a dialog box provided by Qt and then saves it using the BMP_Handler DLL saveBMP function
void Program5::saveFile() 
{
	QString filename = QFileDialog::getSaveFileName(this, tr("Save BMP"), "", tr("Images *.bmp"));
	fname = filename.toLocal8Bit().constData();
	BMP_Handler::saveBMP(fname.c_str(), bits, width, height);
}

//closeFile() will close the file, clear the pictureLabel, reset the capacity and editText box, disable everything, and free the char* array- bits
void Program5::closeFile()
{
	clearImage();
	ui.textEdit->clear();
	QString count = QString::number(0);
	ui.textCounter->setText(count);
	disable();
	ui.textCap->setText(QString::number(0));
	free(bits);
}

//setCounterText() is used to set the text the counts how many characters are in the editText box
void Program5::setCounterText()
{
	int length = ui.textEdit->toPlainText().length();
	QString countValue = QString::number(length);
	ui.textCounter->setText(countValue);
}

//write() is the function that will be called by the write button. it resets the GUI and calls the encode function which encodes the messages
void Program5::write() 
{
	encode();
	ui.textCounter->clear();
	ui.textEdit->clear();
	QString count = QString::number(0);
	ui.textCounter->setText(count);
}

//read() is called by the read button. it will decode the message by calling decode() and updates the counter text
void Program5::read()
{
	decode();
	setCounterText();
}

//clearImage() is a helper function. it clears the image and resets the text. allows for easy multiple usage.
void Program5::clearImage()
{
	ui.pictureLabel->clear();
	QString imageText = "Select \"Open\" from the File Menu";
	ui.pictureLabel->setText(imageText);
}

//encode() does the work of encoding the message from the edit text into the image
//provided separate from write() to all for abstraction and better debugging.
void Program5::encode()
{
	std::string input = ui.textEdit->toPlainText().toStdString();
	const char* message = input.c_str();
	int index = 0;
	for (int i = 0; message[i] != 0x00; i++) {
		for (int j = 0; j < 8; j++) {
			char c = message[i];
			
			//gets the desired bit by moving it to the LSB position and masking it by one
			c = c >> (7 - j);
			c = c & 0x1;
			
			//gets the keep bits by keep everything except for the LSB
			char keepBits = bits[index];
			keepBits = keepBits & 0xFE;

			//puts the keepBits together with the desired LSB
			bits[index] = keepBits | c;
			index++;
		}
	}
	index++;

	//follows the same process as above but encodes a nul terminator so that the decoder can detect when the message has ended
	for (int j = 0; j < 8; j++) {
		char c = 0x00;
		c = c >> (7 - j);
		c = c & 0x1;
		char keepBits = bits[index];
		keepBits = keepBits & 0xFE;
		bits[index] = keepBits | c;
		index++;
	}
}

//decode() does the work of decoding the image to the editText box
//provided separate from write() to all for abstraction and better debugging.
void Program5::decode()
{
	//creates a large enough array of chars
	char* message = new char[(height*width)];
	for (int i = 0; i < (height*width); i++) {
		//prefills the array with nul terminators 
		message[i] = 0x00;
	}
	int index = 0;
	for (int i = 0; i < capacity; i += 8) {
		//probably a poor way of doing this. but it works.
		char one = bits[i];
		char bitOne = one & 0x1;
		char two = bits[i + 1];
		char bitTwo = two & 0x1;
		char three = bits[i + 2];
		char bitThree = three & 0x1;
		char four = bits[i + 3];
		char bitFour = four & 0x1;
		char five = bits[i + 4];
		char bitFive = five & 0x1;
		char six = bits[i + 5];
		char bitSix = six & 0x1;
		char seven = bits[i + 6];
		char bitSeven = seven & 0x1;
		char eight = bits[i + 7];
		char bitEight = eight & 0x1;
		//shifts the bits to the desired position and the OR's all of them together
		message[index] = (bitOne << 7) | (bitTwo << 6) | (bitThree << 5) | (bitFour << 4) | (bitFive << 3) | (bitSix << 2) | (bitSeven << 1) | bitEight;
		if (message[index] == 0x00) {
			//stops at the desired nul terminator
			break;
		}
		index++;
	}
	//converts the chars to a std::string and then to a QString
	std::string out;
	out.assign(message);
	QString output = QString::fromStdString(out);
	ui.textEdit->setPlainText(output);
}

//helper function that enables the text box and buttons
void Program5::enable()
{
	ui.textEdit->setEnabled(true);
	ui.writeButton->setEnabled(true);
	ui.readButton->setEnabled(true);
}

//helper function that disables the text box and buttons
void Program5::disable()
{
	ui.textEdit->setDisabled(true);
	ui.writeButton->setDisabled(true);
	ui.readButton->setDisabled(true);
}

//aboutDialog() uses the built in QMessageBox to open a dialog box for the About window
void Program5::aboutDialog()
{
	QMessageBox msgBox;
	msgBox.setText("This was created for COMP 322 at Grove City College. This was created by Joe Holston. The purpose is to be a steganography frontend program.");
	msgBox.exec();
}