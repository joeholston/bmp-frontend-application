/*
Author: Joe Holston- COMP 322
Program: BMP Frontend Application 
Purpose: Header file for the BMP Frontend Application
Date: November 30, 2017
*/

#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_Program5.h"

class Program5 : public QMainWindow
{
	Q_OBJECT

public:
	Program5(QWidget *parent = Q_NULLPTR);

	void disable();
	void enable();
	void clearImage();
	void encode();
	void decode();

public slots:
	void loadFile();
	void saveFile();
	void closeFile();
	void setCounterText();

	void write();
	void read();

	void aboutDialog();

signals:
	void sendPixmap(QPixmap);

private:
	Ui::Program5Class ui;
	unsigned char* bits;
	int width, height, capacity;
	std::string fname;
};
